#include "StdAfx.h"
#include "NFR_New_Correction_Class.h"

CNFR_New_Correction_Class::CNFR_New_Correction_Class(void)
{
	m_pUAir_Image = m_pUDark_Image = NULL;

	m_pAir_Image = m_pDark_Image = NULL;

	m_pfGain_Map = NULL;

	m_pfMat_R = NULL;

	IMAGE_SIZE_X = IMAGE_SIZE_Y = 0;

	//Smoothing 정도를 결정, 낮을 수록 Smooth
	m_fSmooth_num = 0.0005f;
}


CNFR_New_Correction_Class::~CNFR_New_Correction_Class(void)
{
	if(m_pUAir_Image != NULL)
	{
		delete []m_pUAir_Image;
		m_pUAir_Image = NULL;
	}

	if(m_pUDark_Image != NULL)
	{
		delete []m_pUDark_Image;
		m_pUDark_Image = NULL;
	}

	if(m_pAir_Image != NULL)
	{
		delete []m_pAir_Image;
		m_pAir_Image = NULL;
	}

	if(m_pDark_Image != NULL)
	{
		delete []m_pDark_Image;
		m_pDark_Image = NULL;
	}

	if(m_pfGain_Map != NULL)
	{
		delete []m_pfGain_Map;
		m_pfGain_Map = NULL;
	}

	if(m_pfMat_R != NULL)
	{
		delete []m_pfMat_R;
		m_pfMat_R = NULL;
	}
}

void CNFR_New_Correction_Class::Dispose()
{
	if (m_pUAir_Image != NULL)
	{
		delete[]m_pUAir_Image;
		m_pUAir_Image = NULL;
	}

	if (m_pUDark_Image != NULL)
	{
		delete[]m_pUDark_Image;
		m_pUDark_Image = NULL;
	}

	if (m_pAir_Image != NULL)
	{
		delete[]m_pAir_Image;
		m_pAir_Image = NULL;
	}

	if (m_pDark_Image != NULL)
	{
		delete[]m_pDark_Image;
		m_pDark_Image = NULL;
	}

	if (m_pfGain_Map != NULL)
	{
		delete[]m_pfGain_Map;
		m_pfGain_Map = NULL;
	}

	if (m_pfMat_R != NULL)
	{
		delete[]m_pfMat_R;
		m_pfMat_R = NULL;
	}
}

void CNFR_New_Correction_Class::SetWriteLogProc(WriteLogCallBackProc LoggerProc)
{
	WriteLog = LoggerProc;
}

void CNFR_New_Correction_Class::SetImageSize(int imageWidth, int imageHeight)
{
	IMAGE_SIZE_X = imageWidth;
	IMAGE_SIZE_Y = imageHeight;

	if (imageWidth < 0 || imageHeight < 0)
	{
		CString log;
		log.Format(_T("Invalid Image Size Set : Width = %d, Height = %d"), imageWidth, imageHeight);
		WriteLog(log);
	}
}

void CNFR_New_Correction_Class::SetOffsetImage(unsigned short *pUDark_Image, int imageWidth, int imageHeight)
{
	if (pUDark_Image == NULL)
	{
		WriteLog("Set Offset Image Buffer is NULL");

		return;
	}

	if (IMAGE_SIZE_X != imageWidth || IMAGE_SIZE_Y != imageHeight)
	{
		CString log;
		log.Format(_T("Set Offset Image Different Image Size : Org %d / %d, New %d / %d"),
			IMAGE_SIZE_X, imageWidth, IMAGE_SIZE_Y, imageHeight);
		WriteLog(log);

		return;
	}

	if (m_pUDark_Image != NULL)
	{
		delete[]m_pUDark_Image;
		m_pUDark_Image = NULL;
	}

	m_pUDark_Image = new unsigned short[IMAGE_SIZE_X * IMAGE_SIZE_Y];

	memcpy(m_pUDark_Image, pUDark_Image, sizeof(unsigned short) * IMAGE_SIZE_X * IMAGE_SIZE_Y);
}

void CNFR_New_Correction_Class::SetAirImage(unsigned short *pUAir_Image, int imageWidth, int imageHeight)
{
	if (pUAir_Image == NULL)
	{
		WriteLog("Set Air Image Buffer is NULL");

		return;
	}

	if (IMAGE_SIZE_X != imageWidth || IMAGE_SIZE_Y != imageHeight)
	{
		CString log;
		log.Format(_T("Set Air Image Different Image Size : Org %d / %d, New %d / %d"),
			IMAGE_SIZE_X, imageWidth, IMAGE_SIZE_Y, imageHeight);
		WriteLog(log);

		return;
	}

	if (m_pUAir_Image != NULL)
	{
		delete[]m_pUAir_Image;
		m_pUAir_Image = NULL;
	}

	m_pUAir_Image = new unsigned short[IMAGE_SIZE_X * IMAGE_SIZE_Y];

	memcpy(m_pUAir_Image, pUAir_Image, sizeof(unsigned short) *IMAGE_SIZE_X * IMAGE_SIZE_Y);
}

void CNFR_New_Correction_Class::CreateGainMap()
{
	if (m_pUAir_Image == NULL)
	{
		WriteLog("Create Gain Map - Air Image Buffer is NULL");

		return;
	}

	if (m_pUDark_Image == NULL)
	{
		WriteLog("Create Gain Map - Dark Image Buffer is NULL");

		return;
	}

	if (m_pfGain_Map != NULL)
	{
		delete[]m_pfGain_Map;
		m_pfGain_Map = NULL;
	}

	WriteLog("Allocate Gain Map Buffer");

	m_pfGain_Map = new float[IMAGE_SIZE_X * IMAGE_SIZE_Y];
	memset(m_pfGain_Map, 0, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

	WriteLog("Allocate Off Cur Average Buffer");

	unsigned short *fOff_cor_Averag = NULL;
	fOff_cor_Averag = new unsigned short[IMAGE_SIZE_X * IMAGE_SIZE_Y];
	memset(fOff_cor_Averag, 0, sizeof(unsigned short) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

	float *fReference = NULL, *fFly_Y = NULL, *fFly_X = NULL, *fFly_Trans_Y = NULL;
	float *fFact_R = NULL, *fMat_Q = NULL, *fMat_B = NULL, *fFact_Trans_R = NULL;
	float *fIn_Vector = NULL, *fFly_Temp = NULL, *pfGain_Map = NULL;

	WriteLog("Allocate Reference Buffer");

	fReference = new float[IMAGE_SIZE_X * IMAGE_SIZE_Y];
	memset(fReference, 0, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

	fFly_X = new float[IMAGE_SIZE_X * IMAGE_SIZE_Y];
	memset(fFly_X, 0, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

	fFly_Y = new float[IMAGE_SIZE_X * IMAGE_SIZE_Y];
	memset(fFly_Y, 0, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

	fFly_Trans_Y = new float[IMAGE_SIZE_X * IMAGE_SIZE_Y];
	memset(fFly_Trans_Y, 0, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

	fFact_R = new float[(IMAGE_SIZE_X - 2) * (IMAGE_SIZE_X - 2)];
	memset(fFact_R, 0, sizeof(float) * (IMAGE_SIZE_X - 2) * (IMAGE_SIZE_X - 2));

	fMat_Q = new float[(IMAGE_SIZE_X - 2) * IMAGE_SIZE_X];
	memset(fMat_Q, 0, sizeof(float) * (IMAGE_SIZE_X - 2) * IMAGE_SIZE_X);

	fMat_B = new float[(IMAGE_SIZE_X - 2) * IMAGE_SIZE_X];
	memset(fMat_B, 0, sizeof(float) * (IMAGE_SIZE_X - 2) * IMAGE_SIZE_X);

	fFact_Trans_R = new float[(IMAGE_SIZE_X - 2) * (IMAGE_SIZE_X - 2)];
	memset(fFact_Trans_R, 0, sizeof(float) * (IMAGE_SIZE_X - 2) * (IMAGE_SIZE_X - 2));

	fIn_Vector = new float[IMAGE_SIZE_Y];
	memset(fIn_Vector, 0, sizeof(float) * IMAGE_SIZE_Y);

	fFly_Temp = new float[IMAGE_SIZE_Y];
	memset(fFly_Temp, 0, sizeof(float) * IMAGE_SIZE_Y);

	WriteLog("Calculate Off Cur Average Value");

	// Off_cor_Averge 값 계산
	int iBufSize = IMAGE_SIZE_X * IMAGE_SIZE_Y;

	for (int i = 0; i < iBufSize; i++)
	{
		fOff_cor_Averag[i] = m_pUAir_Image[i] - m_pUDark_Image[i];
	}

	WriteLog("Smooth Spline");

	// Start Fitting//
	x_smooth_spline_pre(fFact_R, fMat_Q, fMat_B, IMAGE_SIZE_X, m_fSmooth_num);

	WriteLog("Fact Trans");

	//fact_R_trans = transpose(fact_R);
	for (int y = 0; y < IMAGE_SIZE_X - 2; y++)
	{
		for (int x = 0; x < IMAGE_SIZE_X - 2; x++)
		{
			fFact_Trans_R[x * (IMAGE_SIZE_X - 2) + y] = fFact_R[y * (IMAGE_SIZE_X - 2) + x];
		}
	}

	WriteLog("Calculate Fly Y Value");

	for (int y = 0; y < IMAGE_SIZE_Y; y++)
	{
		for (int x = 0; x < IMAGE_SIZE_X; x++)
		{
			//in_vector_y = off_cor_avg(:,m);
			fIn_Vector[x] = fOff_cor_Averag[x * IMAGE_SIZE_X + y];
		}
		// fit_y(:,m) = x_smooth_spline(in_vector_y, N, fact_R, fact_R_trans, mat_Q, mat_B);
		x_smooth_spline(fFly_Temp, fIn_Vector, IMAGE_SIZE_X, fFact_R, fFact_Trans_R, fMat_Q, fMat_B);
		for (int k = 0; k < IMAGE_SIZE_X; k++)
		{
			fFly_Y[k * IMAGE_SIZE_X + y] = fFly_Temp[k];
		}
	}

	WriteLog("Calculate Fly Trans Value");

	//fit_y_trans = transpose(fit_y);
	for (int y = 0; y < IMAGE_SIZE_Y; y++)
	{
		for (int x = 0; x < IMAGE_SIZE_X; x++)
		{
			fFly_Trans_Y[x * IMAGE_SIZE_X + y] = fFly_Y[y * IMAGE_SIZE_X + x];
		}
	}

	WriteLog("Calculate Fly X Value");

	memset(fIn_Vector, 0, sizeof(float) * IMAGE_SIZE_Y);
	memset(fFly_Temp, 0, sizeof(float) * IMAGE_SIZE_Y);
	for (int y = 0; y < IMAGE_SIZE_Y; y++)
	{
		for (int x = 0; x < IMAGE_SIZE_X; x++)
		{
			//  in_vector_x = fit_y_trans(:,n);
			fIn_Vector[x] = fFly_Trans_Y[x * IMAGE_SIZE_X + y];
		}

		// fit_x(:,n) = x_smooth_spline(in_vector_x, N, fact_R, fact_R_trans, mat_Q, mat_B);
		x_smooth_spline(fFly_Temp, fIn_Vector, IMAGE_SIZE_X, fFact_R, fFact_Trans_R, fMat_Q, fMat_B);
		for (int k = 0; k < IMAGE_SIZE_X; k++)
		{
			fFly_X[k * IMAGE_SIZE_X + y] = fFly_Temp[k];
		}
	}

	WriteLog("Calculate Reference Value");

	//reference = transpose(fit_x);
	for (int y = 0; y < IMAGE_SIZE_Y; y++)
	{
		for (int x = 0; x < IMAGE_SIZE_X; x++)
		{
			fReference[x * IMAGE_SIZE_X + y] = fFly_X[y * IMAGE_SIZE_X + x];
		}
	}

	WriteLog("Calculate Gain Value");

	//gain_map = (flood_cor_data_avg - dark_data_avg)./reference; 
	for (int i = 0; i < iBufSize; i++)
	{
		m_pfGain_Map[i] = (m_pUAir_Image[i] - m_pUDark_Image[i]) / fReference[i];
	}

	WriteLog("Free All Buffer");

	delete[]fOff_cor_Averag;
	fOff_cor_Averag = NULL;

	delete[]fReference;
	fReference = NULL;

	delete[] fFly_X;
	fFly_X = NULL;

	delete[]fFly_Y;
	fFly_Y = NULL;

	delete[]fFly_Trans_Y;
	fFly_Trans_Y = NULL;

	delete[]fFact_R;
	fFact_R = NULL;

	delete[]fMat_Q;
	fMat_Q = NULL;

	delete[]fMat_B;
	fMat_B = NULL;

	delete[]fFact_Trans_R;
	fFact_Trans_R = NULL;

	delete[]fIn_Vector;
	fIn_Vector = NULL;

	delete[]fFly_Temp;
	fFly_Temp = NULL;
}

void CNFR_New_Correction_Class::SetGainMap(float *gainMap)
{
	if (m_pfGain_Map != NULL)
	{
		delete[]m_pfGain_Map;
		m_pfGain_Map = NULL;
	}

	m_pfGain_Map = new float[IMAGE_SIZE_X * IMAGE_SIZE_Y];

	memset(m_pfGain_Map, 0, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);
	memcpy(m_pfGain_Map, gainMap, sizeof(float) * IMAGE_SIZE_X * IMAGE_SIZE_Y);
/*
	CString log;
	log.Format(_T("Set Gain Map : %.6f   %.6f   %.6f   %.6f   %.6f"),
		m_pfGain_Map[0],
		m_pfGain_Map[1],
		m_pfGain_Map[2],
		m_pfGain_Map[3],
		m_pfGain_Map[4]);
	WriteLog(log);*/
}

float *CNFR_New_Correction_Class::GetGainMap()
{
	CString log;
	log.Format(_T("Get Gain Map : %.6f   %.6f   %.6f   %.6f   %.6f"),
		m_pfGain_Map[0],
		m_pfGain_Map[1],
		m_pfGain_Map[2],
		m_pfGain_Map[3],
		m_pfGain_Map[4]);
	WriteLog(log);

	return m_pfGain_Map;
}

void CNFR_New_Correction_Class::x_smooth_spline_pre(float *fFact_R, float *fMat_Q, float *fMat_B, int iRow, float fSmooth_Num)
{
	int *iX_Range = NULL, *iSigma = NULL;
	float *h = NULL, *p = NULL, *r = NULL, *f = NULL;
	float *fMat_R = NULL, *fInverse_Mat_Q = NULL, *fMat_A = NULL;
	float fTemp = 0.0f, *fTemp_01 = NULL, *fTemp_02 = NULL, fmu = 0.0f;

	iX_Range = new int[iRow];
	memset(iX_Range, 0, sizeof(int) * iRow);

	iSigma = new int[iRow * iRow];
	memset(iSigma, 0, sizeof(int) * iRow * iRow);

	h = new float[iRow - 1];
	memset(h, 0, sizeof(float) * (iRow - 1));

	p = new float[iRow - 1];
	memset(p, 0, sizeof(float) * (iRow - 1));

	r = new float[iRow - 1];
	memset(r, 0, sizeof(float) * (iRow - 1));

	f = new float[iRow - 1];
	memset(f, 0, sizeof(float) * (iRow - 1));

	fMat_R = new float[(iRow - 2) * (iRow - 2)];
	memset(fMat_R, 0, sizeof(float) * (iRow - 2) * (iRow - 2));

	fInverse_Mat_Q = new float[(iRow - 2) * iRow];
	memset(fInverse_Mat_Q, 0, sizeof(float) * (iRow - 2) * iRow);

	fMat_A = new float[(iRow - 2) * (iRow - 2)];
	memset(fMat_A, 0, sizeof(float) * (iRow - 2) * (iRow - 2));

	fTemp_01 = new float[(iRow - 2) * iRow];
	memset(fTemp_01, 0, sizeof(float) * (iRow - 2) * iRow);

	fTemp_02 = new float[(iRow - 2) * (iRow - 2)];
	memset(fTemp_02, 0, sizeof(float) * (iRow - 2) * (iRow - 2));

	for (int i = 0; i < iRow; i++)
	{
		iX_Range[i] = i + 1;
	}

	for (int i = 0; i < iRow - 1; i++)
	{
		h[i] = iX_Range[i + 1] - iX_Range[i];
		r[i] = 3.0 / h[i];

		if (i > 0)
		{
			p[i] = 2.0 * (h[i - 1] + h[i]);
			f[i] = -(r[i - 1] + r[i]);
		}
	}

	for (int y = 0; y < iRow - 2; y++)
	{
		for (int x = 0; x < iRow - 2; x++)
		{
			if (x == 0 && y == 0)
			{
				fMat_R[y * (iRow - 2) + x] = p[x + 1];
				fMat_R[y * (iRow - 2) + (x + 1)] = h[x + 1];
			}
			else if (x == iRow - 3 && y == iRow - 3)
			{
				fMat_R[y * (iRow - 2) + (x - 1)] = h[x];
				fMat_R[y * (iRow - 2) + x] = p[x + 1];
			}
			else
			{
				if (x == y)
				{
					fMat_R[y * (iRow - 2) + (y - 1)] = h[y];
					fMat_R[y * (iRow - 2) + y] = p[y + 1];
					fMat_R[y * (iRow - 2) + (y + 1)] = h[y + 1];
				}
			}

		}
		fMat_Q[y * iRow + y] = r[y];
		fMat_Q[y * iRow + (y + 1)] = f[y + 1];
		fMat_Q[y * iRow + (y + 2)] = r[y + 1];
	}

	fmu = (2.0 * (1 - fSmooth_Num)) / (3.0 * fSmooth_Num);

	for (int y = 0; y < iRow; y++)
	{
		for (int x = 0; x < iRow; x++)
		{
			if (x == y)
			{
				iSigma[y * iRow + x] = 1;
			}
		}
	}

	//transpose(mat_Q`)
	for (int y = 0; y < iRow - 2; y++)
	{
		for (int x = 0; x < iRow; x++)
		{
			fInverse_Mat_Q[x * (iRow - 2) + y] = fMat_Q[y * iRow + x];
		}
	}

	// 행렬곱(mat_Q * Sigma)
	for (int y = 0; y < iRow - 2; y++)
	{
		for (int n = 0; n < iRow; n++)
		{
			for (int x = 0; x < iRow; x++)
			{
				fTemp = fTemp + (fMat_Q[y * iRow + x] * iSigma[x * iRow + n]);
			}
			fTemp_01[y * iRow + n] = fTemp;
			fTemp = 0.0f;
		}
	}

	fTemp = 0.0f;
	// 행렬곱 (mat_Q * Sigma) *  *  mat_Q'
	for (int y = 0; y < iRow - 2; y++)
	{
		for (int n = 0; n < iRow - 2; n++)
		{
			for (int x = 0; x < iRow; x++)
			{
				fTemp = fTemp + (fTemp_01[y * iRow + x] * fInverse_Mat_Q[x * (iRow - 2) + n]);
			}
			fTemp_02[y * (iRow - 2) + n] = fTemp;
			fTemp = 0.0f;
		}
	}

	//((mat_Q * Sigma) *  *  mat_Q') * mu + mat_R
	for (int i = 0; i < (iRow - 2) * (iRow - 2); i++)
	{
		fMat_A[i] = (fTemp_02[i] * fmu) + fMat_R[i];
	}
	//mat_B = Sigma * mat_Q'.* mu;
	fTemp = 0.0f;
	for (int y = 0; y < iRow; y++)
	{
		for (int n = 0; n < iRow - 2; n++)
		{
			for (int x = 0; x < iRow; x++)
			{
				fTemp = fTemp + (iSigma[y * iRow + x] * fInverse_Mat_Q[x * (iRow - 2) + n]);
			}
			fMat_B[y * (iRow - 2) + n] = fTemp * fmu;

			fTemp = 0.0f;
		}
	}

	// Cholesky Factorization
	for (int y = 0; y < iRow - 2; y++)
	{
		for (int x = 0; x < iRow - 2; x++)
		{
			if (x >= y)
			{
				fFact_R[y * (iRow - 2) + x] = fMat_A[y * (iRow - 2) + x];
			}
		}
	}

	fTemp = 0.0f;

	float ftemp_00 = 0.0f;
	for (int i = 0; i < iRow - 2; i++)
	{
		for (int j = i + 1; j < iRow - 2; j++)
		{
			fTemp = fFact_R[i * (iRow - 2) + j] / fFact_R[i * (iRow - 2) + i];
			for (int k = j; k < iRow - 2; k++)
			{
				ftemp_00 = fFact_R[i * (iRow - 2) + k] * fTemp;

				fFact_R[j * (iRow - 2) + k] -= ftemp_00;
				// fFact_R[j * (iRow - 2) + k]
				ftemp_00 = 0.0f;
			}
			fTemp = 0.0f;
		}
		fTemp = sqrt(fFact_R[i * (iRow - 2) + i]);
		for (int k = i; k < iRow - 2; k++)
		{
			fFact_R[i * (iRow - 2) + k] /= fTemp;
			//fFact_R[i * (iRow - 2) + k] 
		}
		fTemp = 0.0f;
	}
	// 	fTemp = 0.0f;
	// 	for(int i = 0; i < iRow - 2; i++)
	// 	{
	// 		fTemp = sqrt(fFact_R[i * (iRow - 2) + i]);
	// 		for(int k = i; k < iRow -2; k++)
	// 		{			
	// 			fFact_R[i * (iRow - 2) + k] = fFact_R[i * (iRow - 2) + k] / fTemp;
	// 		}
	// 		
	// 	}
	delete[]iX_Range;
	iX_Range = NULL;

	delete[]iSigma;
	iSigma = NULL;

	delete[]h;
	h = NULL;

	delete[]p;
	p = NULL;

	delete[]r;
	r = NULL;

	delete[]f;
	f = NULL;

	delete[]fInverse_Mat_Q;
	fInverse_Mat_Q = NULL;

	delete[]fMat_A;
	fMat_A = NULL;

	delete[]fTemp_01;
	fTemp_01 = NULL;

	delete[]fTemp_02;
	fTemp_02 = NULL;

	delete[]fMat_R;
	fMat_R = NULL;
}

void CNFR_New_Correction_Class::x_smooth_spline(float *fData, float *fIn_Vector, int iRow, float *fFect_R, float *fact_R_trans, float *fMat_Q, float *fMat_B)
{
	float *pfCoef_b_Semi = NULL, *pfCoef_d = NULL, *pfValue_X = NULL, *pfTemp_Mat = NULL;
	float fTemp = 0.0f;
	double *pfCoef_b = NULL;
	pfCoef_b_Semi = new float[iRow - 2];
	memset(pfCoef_b_Semi, 0, sizeof(float) * (iRow - 2));

	pfCoef_b = new double[iRow - 2];
	memset(pfCoef_b, 0, sizeof(double) * (iRow - 2));

	pfCoef_d = new float[iRow];
	memset(pfCoef_b, 0, sizeof(float) * iRow);

	pfValue_X = new float[iRow - 2];
	memset(pfValue_X, 0, sizeof(float) * (iRow - 2));

	pfTemp_Mat = new float[iRow];
	memset(pfTemp_Mat, 0, sizeof(float) * iRow);

	// value_x = mat_Q * in_vector;
	for (int y = 0; y < iRow - 2; y++)
	{
		for (int n = 0; n < 1; n++)
		{
			for (int x = 0; x < iRow; x++)
			{
				fTemp = fTemp + (fMat_Q[y * iRow + x] * fIn_Vector[x]);
			}
			pfValue_X[y] = fTemp;

			fTemp = 0.0f;
		}
	}
	fTemp = 0.0f;
	//Forward substitution
	for (int y = 0; y < iRow - 2; y++)
	{
		pfCoef_b_Semi[y] = pfValue_X[y];
		if (y > 0)
		{
			if (y == 1)
			{
				pfCoef_b_Semi[y] = pfCoef_b_Semi[y] - (fact_R_trans[y * (iRow - 2)] * pfCoef_b_Semi[0]);
			}
			else
			{
				for (int x = 0; x <= y - 1; x++)
				{
					pfCoef_b_Semi[y] = pfCoef_b_Semi[y] - (fact_R_trans[y * (iRow - 2) + x] * pfCoef_b_Semi[x]);
				}
			}
		}
		pfCoef_b_Semi[y] = pfCoef_b_Semi[y] / fact_R_trans[y * (iRow - 2) + y];
	}
	//Backward substitution
	for (int y = 0; y < iRow - 2; y++)
	{
		pfCoef_b[iRow - 3 - y] = pfCoef_b_Semi[iRow - 3 - y];
		if (y > 0)
		{
			if (y == 1)
			{
				pfCoef_b[iRow - 3 - y] = pfCoef_b[iRow - 3 - y] - (fFect_R[(iRow - 3 - y) * (iRow - 2) + (iRow - 3)] * pfCoef_b[iRow - 3]);
			}
			else
			{
				for (int x = 0; x <= y - 1; x++)
				{
					pfCoef_b[iRow - 3 - y] = pfCoef_b[iRow - 3 - y] - (fFect_R[(iRow - 3 - y) * (iRow - 2) + (iRow - 3 - x)] * pfCoef_b[iRow - 3 - x]);
				}
			}
		}
		pfCoef_b[iRow - 3 - y] = pfCoef_b[iRow - 3 - y] / fFect_R[(iRow - 3 - y) * (iRow - 2) + (iRow - 3 - y)];
	}

	fTemp = 0.0f;
	//mat_temp = mat_B * coef_b';
	for (int y = 0; y < iRow; y++)
	{
		for (int n = 0; n < 1; n++)
		{
			for (int x = 0; x < iRow - 2; x++)
			{
				fTemp = fTemp + (fMat_B[y * (iRow - 2) + x] * pfCoef_b[x]);
			}
			pfTemp_Mat[y] = fTemp;

			fTemp = 0.0f;
		}
	}

	for (int i = 0; i < iRow; i++)
	{
		fData[i] = pfCoef_d[i] = fIn_Vector[i] - pfTemp_Mat[i];
	}

	delete[]pfCoef_b_Semi;
	pfCoef_b_Semi = NULL;

	delete[]pfCoef_b;
	pfCoef_b = NULL;

	delete[]pfCoef_d;
	pfCoef_d = NULL;

	delete[]pfValue_X;
	pfValue_X = NULL;

	delete[]pfTemp_Mat;
	pfTemp_Mat = NULL;
}

void CNFR_New_Correction_Class::FlatFieldCorrection(unsigned short *pUImage_Src, unsigned short *pUImage_Dst, int iImage_Size_X, int iImage_Size_Y)
{
	int iBuf_Size = 0;

	if (pUImage_Src == NULL)
	{
		WriteLog("Flat Field Correction - Image Source Buffer is NULL");

		return;
	}
	if(IMAGE_SIZE_X != iImage_Size_X || IMAGE_SIZE_Y != iImage_Size_Y)
	{
		CString log;
		log.Format(_T("Flat Field Correction Different Image Size : Org %d / %d, New %d / %d"),
			IMAGE_SIZE_X, iImage_Size_X, IMAGE_SIZE_Y, iImage_Size_Y);
		WriteLog(log);

		return;
	}

	iBuf_Size = IMAGE_SIZE_X * IMAGE_SIZE_Y;

	//WriteLog("Calculate Correction Value");

	int fTemp = 0.0f;
	for(int iCorrection = 0; iCorrection < iBuf_Size; iCorrection++)
	{
		fTemp = (pUImage_Src[iCorrection] - m_pUDark_Image[iCorrection]) / m_pfGain_Map[iCorrection];

		pUImage_Src[iCorrection] = (int)fTemp;
	}
}

void CNFR_New_Correction_Class::ApplySplineSmooth(unsigned short *pUImage_Src, int iImage_Size_X, int iImage_Size_Y)
{
	float *fFly_Y = NULL, *fFly_X = NULL, *fFly_Trans_Y = NULL;
	float *fFact_R = NULL, *fMat_Q = NULL, *fMat_B = NULL, *fFact_Trans_R = NULL;
	float *fIn_Vector = NULL, *fFly_Temp = NULL, *pfGain_Map = NULL;

	WriteLog("Allocate Reference Buffer");

	fFly_X = new float[iImage_Size_X * iImage_Size_Y];
	memset(fFly_X, 0, sizeof(float) * iImage_Size_X * iImage_Size_Y);

	fFly_Y = new float[iImage_Size_X * iImage_Size_Y];
	memset(fFly_Y, 0, sizeof(float) * iImage_Size_X * iImage_Size_Y);

	fFly_Trans_Y = new float[iImage_Size_X * iImage_Size_Y];
	memset(fFly_Trans_Y, 0, sizeof(float) * iImage_Size_X * iImage_Size_Y);

	fFact_R = new float[(iImage_Size_X - 2) * (iImage_Size_X - 2)];
	memset(fFact_R, 0, sizeof(float) * (iImage_Size_X - 2) * (iImage_Size_X - 2));

	fMat_Q = new float[(iImage_Size_X - 2) * iImage_Size_X];
	memset(fMat_Q, 0, sizeof(float) * (iImage_Size_X - 2) * iImage_Size_X);

	fMat_B = new float[(iImage_Size_X - 2) * iImage_Size_X];
	memset(fMat_B, 0, sizeof(float) * (iImage_Size_X - 2) * iImage_Size_X);

	fFact_Trans_R = new float[(iImage_Size_X - 2) * (iImage_Size_X - 2)];
	memset(fFact_Trans_R, 0, sizeof(float) * (iImage_Size_X - 2) * (iImage_Size_X - 2));

	fIn_Vector = new float[iImage_Size_Y];
	memset(fIn_Vector, 0, sizeof(float) * iImage_Size_Y);

	fFly_Temp = new float[iImage_Size_Y];
	memset(fFly_Temp, 0, sizeof(float) * iImage_Size_Y);

	WriteLog("Calculate Off Cur Average Value");

	// Off_cor_Averge 값 계산
	int iBufSize = iImage_Size_X * iImage_Size_Y;

	WriteLog("Smooth Spline");

	// Start Fitting//
	x_smooth_spline_pre(fFact_R, fMat_Q, fMat_B, iImage_Size_X, m_fSmooth_num);

	WriteLog("Fact Trans");

	//fact_R_trans = transpose(fact_R);
	for (int y = 0; y < iImage_Size_X - 2; y++)
	{
		for (int x = 0; x < iImage_Size_X - 2; x++)
		{
			fFact_Trans_R[x * (iImage_Size_X - 2) + y] = fFact_R[y * (iImage_Size_X - 2) + x];
		}
	}

	WriteLog("Calculate Fly Y Value");

	for (int y = 0; y < iImage_Size_Y; y++)
	{
		for (int x = 0; x < iImage_Size_X; x++)
		{
			//in_vector_y = off_cor_avg(:,m);
			fIn_Vector[x] = pUImage_Src[x * iImage_Size_X + y];
		}
		// fit_y(:,m) = x_smooth_spline(in_vector_y, N, fact_R, fact_R_trans, mat_Q, mat_B);
		x_smooth_spline(fFly_Temp, fIn_Vector, iImage_Size_X, fFact_R, fFact_Trans_R, fMat_Q, fMat_B);
		for (int k = 0; k < iImage_Size_X; k++)
		{
			fFly_Y[k * iImage_Size_X + y] = fFly_Temp[k];
		}
	}

	WriteLog("Calculate Fly Trans Value");

	//fit_y_trans = transpose(fit_y);
	for (int y = 0; y < iImage_Size_Y; y++)
	{
		for (int x = 0; x < iImage_Size_X; x++)
		{
			fFly_Trans_Y[x * iImage_Size_X + y] = fFly_Y[y * iImage_Size_X + x];
		}
	}

	WriteLog("Calculate Fly X Value");

	memset(fIn_Vector, 0, sizeof(float) * iImage_Size_Y);
	memset(fFly_Temp, 0, sizeof(float) * iImage_Size_Y);
	for (int y = 0; y < iImage_Size_Y; y++)
	{
		for (int x = 0; x < iImage_Size_X; x++)
		{
			//  in_vector_x = fit_y_trans(:,n);
			fIn_Vector[x] = fFly_Trans_Y[x * iImage_Size_X + y];
		}

		// fit_x(:,n) = x_smooth_spline(in_vector_x, N, fact_R, fact_R_trans, mat_Q, mat_B);
		x_smooth_spline(fFly_Temp, fIn_Vector, iImage_Size_X, fFact_R, fFact_Trans_R, fMat_Q, fMat_B);
		for (int k = 0; k < iImage_Size_X; k++)
		{
			fFly_X[k * iImage_Size_X + y] = fFly_Temp[k];
		}
	}

	WriteLog("Calculate Reference Value");

	//reference = transpose(fit_x);
	for (int y = 0; y < iImage_Size_Y; y++)
	{
		for (int x = 0; x < iImage_Size_X; x++)
		{
			pUImage_Src[x * iImage_Size_X + y] = fFly_X[y * iImage_Size_X + x];
		}
	}

	WriteLog("Free All Buffer");

	delete[] fFly_X;
	fFly_X = NULL;

	delete[]fFly_Y;
	fFly_Y = NULL;

	delete[]fFly_Trans_Y;
	fFly_Trans_Y = NULL;

	delete[]fFact_R;
	fFact_R = NULL;

	delete[]fMat_Q;
	fMat_Q = NULL;

	delete[]fMat_B;
	fMat_B = NULL;

	delete[]fFact_Trans_R;
	fFact_Trans_R = NULL;

	delete[]fIn_Vector;
	fIn_Vector = NULL;

	delete[]fFly_Temp;
	fFly_Temp = NULL;
}