#include "stdafx.h"

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

[assembly:AssemblyTitleAttribute(L"Correction Library")];
[assembly:AssemblyDescriptionAttribute(L"")];
[assembly:AssemblyConfigurationAttribute(L"")];
[assembly:AssemblyCompanyAttribute(L"NanoFocusRay Co.,Ltd")];
[assembly:AssemblyProductAttribute(L"Correction Library")];
[assembly:AssemblyCopyrightAttribute(L"Copyright (C)  2019 NanoFocusRay Co.,Ltd")];
[assembly:AssemblyTrademarkAttribute(L"")];
[assembly:AssemblyCultureAttribute(L"")];

[assembly:AssemblyVersionAttribute("1.0.0.1")];

[assembly:ComVisible(false)];

[assembly:CLSCompliantAttribute(true)];
