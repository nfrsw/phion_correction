﻿#pragma once
#include "NFR_New_Correction_Class.h"
#include <msclr\marshal_cppstd.h>

#define _AFXDLL
#include <afx.h>

using namespace System;
using namespace msclr::interop;
using namespace System::Runtime::InteropServices;

namespace NFRCorrectionCli {
	public ref class CNFR_New_Correction_ClassWrap
	{
	protected:
		CNFR_New_Correction_Class *m_pCorrection;

	private:
		int IMAGE_SIZE_X = 0;
		int IMAGE_SIZE_Y = 0;


	public:
		CNFR_New_Correction_ClassWrap(void);
		~CNFR_New_Correction_ClassWrap(void);
		
		delegate void WriteLogHandler(String^ logText);
		delegate void WriteLogProc(CString logText);
		WriteLogHandler^ Logger;
		void WriteLog(CString logText);
		void RegistWriteLogHandler(WriteLogHandler^ handler);

		void SetImageSize(int imageWidth, int imageHeight);
		void SetOffsetImage(cli::array<unsigned short>^ darkImageBuffer, int imageWidth, int imageHeight);
		void SetAirImage(cli::array<unsigned short>^ airImageBuffer, int imageWidth, int imageHeight);
		void CreateGainMap();
		void SetGainMap(cli::array<float>^ gainMap);
		cli::array<float>^ GetGainMap(int imageWidth, int imageHeight);
		cli::array<short>^ FlatFieldCorrection(cli::array<unsigned short>^ sourceImageBuffer, int imageWidth, int imageHeight);
		cli::array<short>^ ApplySplineSmooth(cli::array<unsigned short>^ sourceImageBuffer, int imageWidth, int imageHeight);
	};
}
