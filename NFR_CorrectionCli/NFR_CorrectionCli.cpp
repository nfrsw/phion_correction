#include "stdafx.h"
#include <string.h>
#include "NFR_CorrectionCli.h"

namespace NFRCorrectionCli
{
	static GCHandle writeLogRequestedHandler;

	CNFR_New_Correction_ClassWrap::CNFR_New_Correction_ClassWrap(void)
	{
		if (m_pCorrection == NULL)
		{
			m_pCorrection = new CNFR_New_Correction_Class();
		}
	}

	CNFR_New_Correction_ClassWrap::~CNFR_New_Correction_ClassWrap(void)
	{
		WriteLog("Correction Library Destroyed");

		if (m_pCorrection)
		{
			delete m_pCorrection;
			m_pCorrection = 0;
		}
		
		writeLogRequestedHandler.Free();
	}

	void CNFR_New_Correction_ClassWrap::WriteLog(CString logText)
	{
		if (this->Logger == nullptr)
		{
			return;
		}

		this->Logger(gcnew String(logText));
	}

	void CNFR_New_Correction_ClassWrap::RegistWriteLogHandler(WriteLogHandler^ handler)
	{
		this->Logger = handler;

		WriteLogProc^ WriteLogRequestedDelegatePointer = gcnew WriteLogProc(this, &CNFR_New_Correction_ClassWrap::WriteLog);
		writeLogRequestedHandler = GCHandle::Alloc(WriteLogRequestedDelegatePointer);
		IntPtr WriteLogRequestedManagedFunction = Marshal::GetFunctionPointerForDelegate(WriteLogRequestedDelegatePointer);
		WriteLogCallBackProc WriteLogRequestedCallback = static_cast<WriteLogCallBackProc>(WriteLogRequestedManagedFunction.ToPointer());

		m_pCorrection->SetWriteLogProc(WriteLogRequestedCallback);
	}

	void CNFR_New_Correction_ClassWrap::SetImageSize(int imageWidth, int imageHeight)
	{
		m_pCorrection->SetImageSize(imageWidth, imageHeight);
	}

	void CNFR_New_Correction_ClassWrap::SetOffsetImage(cli::array<unsigned short>^ darkImageBuffer, int imageWidth, int imageHeight)
	{
		pin_ptr<unsigned short> pDarkImageBufferMem = &darkImageBuffer[0];

		m_pCorrection->SetOffsetImage(pDarkImageBufferMem, imageWidth, imageHeight);

		/*CString log;
		log.Format(_T("Set Offset Image : %d %d"), imageWidth, imageHeight);
		WriteLog(log);*/
	}

	void CNFR_New_Correction_ClassWrap::SetAirImage(cli::array<unsigned short>^ airImageBuffer, int imageWidth, int imageHeight)
	{
		pin_ptr<unsigned short> pAirImageBufferMem = &airImageBuffer[0];

		m_pCorrection->SetAirImage(pAirImageBufferMem, imageWidth, imageHeight);

		/*CString log;
		log.Format(_T("Set Air Image : %d %d"), imageWidth, imageHeight);
		WriteLog(log);*/
	}

	void CNFR_New_Correction_ClassWrap::SetGainMap(cli::array<float>^ gainMap)
	{
		pin_ptr<float> pGainMapMem = &gainMap[0];

		m_pCorrection->SetGainMap(pGainMapMem);
	}

	cli::array<float>^ CNFR_New_Correction_ClassWrap::GetGainMap(int imageWidth, int imageHeight)
	{
		float *pfGain_Map = m_pCorrection->GetGainMap();

		int imageLength = imageWidth * imageHeight;

		cli::array<float>^ gainMapTemp = gcnew cli::array<float>(imageLength);

		Marshal::Copy((IntPtr)pfGain_Map, gainMapTemp, 0, imageLength);

		return gainMapTemp;
	}

	void CNFR_New_Correction_ClassWrap::CreateGainMap()
	{
		m_pCorrection->CreateGainMap();
	}

	cli::array<short>^ CNFR_New_Correction_ClassWrap::FlatFieldCorrection(cli::array<unsigned short>^ sourceImageBuffer, int imageWidth, int imageHeight)
	{
		/*CString log;
		log.Format(_T("Call Flat Field Correction : %d %d"), imageWidth, imageHeight);
		WriteLog(log);*/

		pin_ptr<unsigned short> pSourceImageBufferMem = &sourceImageBuffer[0];

		CFileFind pFind;
		BOOL bFindFlag = FALSE;
		unsigned short *pImage = NULL;
		CString str_Temp;
		CStringArray str_Image_Path_List;

		int IMAGE_SIZE_X = imageWidth;
		int IMAGE_SIZE_Y = imageHeight;

		pImage = new unsigned short[IMAGE_SIZE_X * IMAGE_SIZE_Y];
		memset(pImage, 0, sizeof(unsigned short) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

		int imageLength = imageWidth * imageHeight;

		memcpy(pImage, pSourceImageBufferMem, imageLength * sizeof(unsigned short));
		
		m_pCorrection->FlatFieldCorrection(pImage, NULL, IMAGE_SIZE_X, IMAGE_SIZE_Y);


		cli::array<short>^ outputImageBufferTemp = gcnew cli::array<short>(imageWidth * imageHeight);

		Marshal::Copy((IntPtr)pImage, outputImageBufferTemp, 0, imageWidth * imageHeight);

		delete[]pImage;
		pImage = NULL;

		return outputImageBufferTemp;
	}


	cli::array<short>^ CNFR_New_Correction_ClassWrap::ApplySplineSmooth(cli::array<unsigned short>^ sourceImageBuffer, int imageWidth, int imageHeight)
	{
		pin_ptr<unsigned short> pSourceImageBufferMem = &sourceImageBuffer[0];

		CFileFind pFind;
		BOOL bFindFlag = FALSE;
		unsigned short *pImage = NULL;
		CString str_Temp;
		CStringArray str_Image_Path_List;

		int IMAGE_SIZE_X = imageWidth;
		int IMAGE_SIZE_Y = imageHeight;

		pImage = new unsigned short[IMAGE_SIZE_X * IMAGE_SIZE_Y];
		memset(pImage, 0, sizeof(unsigned short) * IMAGE_SIZE_X * IMAGE_SIZE_Y);

		int imageLength = imageWidth * imageHeight;

		memcpy(pImage, pSourceImageBufferMem, imageLength * sizeof(unsigned short));

		m_pCorrection->ApplySplineSmooth(pImage, IMAGE_SIZE_X, IMAGE_SIZE_Y);


		cli::array<short>^ outputImageBufferTemp = gcnew cli::array<short>(imageWidth * imageHeight);

		Marshal::Copy((IntPtr)pImage, outputImageBufferTemp, 0, imageWidth * imageHeight);

		delete[]pImage;
		pImage = NULL;

		return outputImageBufferTemp;
	}
}
