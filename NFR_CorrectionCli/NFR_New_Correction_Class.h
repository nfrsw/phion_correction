#pragma once

typedef void(_stdcall *WriteLogCallBackProc)(CString);

class CNFR_New_Correction_Class
{
public:
	CNFR_New_Correction_Class(void);
	~CNFR_New_Correction_Class(void);

	unsigned short *m_pUAir_Image, *m_pUDark_Image;
	short *m_pAir_Image, *m_pDark_Image;

	float *m_pfGain_Map, *m_pfMat_R;

	int IMAGE_SIZE_X, IMAGE_SIZE_Y;

	float m_fSmooth_num;

	void SetWriteLogProc(WriteLogCallBackProc LoggerProc);
	WriteLogCallBackProc WriteLog;

	void Dispose();

	void SetImageSize(int imageWidth, int imageHeight);
	void SetOffsetImage(unsigned short *pUDark_Image, int imageWidth, int imageHeight);
	void SetAirImage(unsigned short *pUAir_Image, int imageWidth, int imageHeight);
	void CreateGainMap();
	void SetGainMap(float *gainMap);
	float *GetGainMap();
	void FlatFieldCorrection(unsigned short *pUImage_Src, unsigned short *pUImage_Dst, int iImage_Size_X, int iImage_Size_Y);
	void ApplySplineSmooth(unsigned short *pUImage_Src, int iImage_Size_X, int iImage_Size_Y);

	void x_smooth_spline_pre(float *fFact_R, float *fMat_Q, float *fMat_B, int iRow, float fSmooth_Num);
	void x_smooth_spline(float *fData, float *fIn_Vector, int iRow, float *fFect_R, float *fact_R_trans, float *fMat_Q, float *fMat_B);
};

