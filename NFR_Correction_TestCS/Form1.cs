﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NFR_Correction_TestCS
{
    public partial class Form1 : Form
    {
        private static int _nSuccessCnt = 0;

        public Form1()
        {
            InitializeComponent();

            txtAirInput.Text = @"D:\Temp\Correction\Air\Air.raw";
            txtOffsetInput.Text = @"D:\Temp\Correction\Offset\Offset.raw";
            txtImgInput.Text = @"D:\Temp\Correction\Output";
        }

        public void CallbackHandler(int nTotalCnt, int nCurCnt, bool bSendSuccess)
        {
            this.Invoke(new Action(
                delegate ()
                {
                    progressBar1.Minimum = 1;
                    progressBar1.Maximum = nTotalCnt;
                    progressBar1.Value = nCurCnt;

                    if (bSendSuccess)
                    {
                        _nSuccessCnt++;
                    }

                    label4.Text = _nSuccessCnt + "  of  " + nTotalCnt;
                }));
            
            if (progressBar1.Value == nTotalCnt)
            {
                MessageBox.Show("Finish!!");
                _nSuccessCnt = 0;
                return;
            }
        }

        NFRCorrectionCli.CNFR_New_Correction_ClassWrap correction = new NFRCorrectionCli.CNFR_New_Correction_ClassWrap();

        private delegate void WriteLogDelegate(string logText);

        public void WriteLog(string logText)
        {
            try
            {
                string debugLog = string.Format("[{0}] {1}\r\n", DateTime.Now.ToString("HH:mm:ss"), logText);

                txtLog.Invoke((MethodInvoker)delegate
                {
                    txtLog.Text += debugLog;

                    txtLog.SelectionStart = txtLog.TextLength;
                    txtLog.ScrollToCaret();
                });
            }
            catch (Exception)
            {
            }
        }

        public static void MakesureDirectoryExists(string path)
        {
            if (Directory.Exists(path) == false)
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                }
        }

        void DoCorrection()
        {
            int width = ToInteger(txtImageWidth.Text);
            int height = ToInteger(txtImageHeight.Text);

            SetAirImage();

            SetOffsetImage();

            SetGainMap();

            List<ushort[]> inputRawFiles = ReadAllRawFiles(txtImgInput.Text);

            DateTime dateTime = DateTime.Now;

            MakesureDirectoryExists(txtImgOutput.Text);

            int outputIndex = 0;

            foreach (ushort[] inputBuffer in inputRawFiles)
            {
                short[] correctedBuffer = correction.FlatFieldCorrection(inputBuffer, width, height);

                ushort[] finalImageBuffer = new ushort[inputBuffer.Length];

                Buffer.BlockCopy(correctedBuffer, 0, finalImageBuffer, 0, inputBuffer.Length * 2);

                string newFileName = string.Format("{0}\\{1}_{2:D4}.raw", txtImgOutput.Text, dateTime.ToString("yyyyMMdd_HHmmss"), outputIndex);

                WriteBufferToFile(finalImageBuffer, newFileName);

                outputIndex++;
            }

            correction.Dispose();
        }

        private void Correction_WriteLogHandler2(object A_0)
        {
            throw new NotImplementedException();
        }

        private void Correction_WriteLogHandler1(object A_0)
        {
            throw new NotImplementedException();
        }

        private void Correction_WriteLogHandler(object A_0)
        {
            throw new NotImplementedException();
        }

        void CorrectionTestLoop(int loopCount)
        {
            int testCount = 0;

            while (testCount < loopCount)
            {
                if (testCount % 5 == 0)
                {
                    string gainMapFilePath = GetGainMapFilePath(txtImgInput.Text);

                    File.Delete(gainMapFilePath);
                }

                DoCorrection();

                testCount++;
            }
        }

        void func()
        {
            DoCorrection();

            //CorrectionTestLoop(20000);

            MessageBox.Show("Finished");
        }

        private string GetGainMapFilePath(string filePath)
        {
            string folderPath = Path.GetDirectoryName(filePath);

            string gainMapFilePath = string.Format("{0}\\GainMap.txt", folderPath);

            return gainMapFilePath;
        }

        private string ChangeFileNameAppendix(string filePath)
        {
            string fileName = Path.GetFileNameWithoutExtension(filePath);

            string newFileName = fileName + "_C.raw";

            string folderPath = Path.GetDirectoryName(filePath);

            string newFolderPath = string.Format("{0}\\Completed", folderPath);

            Directory.CreateDirectory(newFolderPath);

            return string.Format("{0}\\{1}", newFolderPath, newFileName);
        }

        private ushort[] ReadBufferFromFile(string filePath)
        {
            FileStream fileStream = null;
            BinaryReader binaryReader = null;

            byte[] imageBuffer = null;
            ushort[] imageBufferInShort = null;

            fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            binaryReader = new BinaryReader(fileStream);

            imageBuffer = binaryReader.ReadBytes((int)fileStream.Length);

            imageBufferInShort = new ushort[imageBuffer.Length / 2];

            Buffer.BlockCopy(imageBuffer, 0, imageBufferInShort, 0, imageBuffer.Length);

            binaryReader.Close();
            fileStream.Close();

            return imageBufferInShort;
        }

        private void WriteBufferToFile(string filePath, short[] imageBuffer)
        {
            FileStream fileStream = null;
            BinaryWriter binaryWriter = null;

            byte[] imageBufferInByte = null;

            fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            binaryWriter = new BinaryWriter(fileStream);

            imageBufferInByte = new byte[imageBuffer.Length * 2];

            Buffer.BlockCopy(imageBuffer, 0, imageBufferInByte, 0, imageBufferInByte.Length);

            binaryWriter.Write(imageBufferInByte);

            binaryWriter.Close();
            fileStream.Close();
        }

        private double[] ReadGainMapFromFile(string filePath)
        {
            FileStream fileStream = null;
            StreamReader streamReader = null;

            string gainMapText = string.Empty;

            fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            streamReader = new StreamReader(fileStream);

            gainMapText = streamReader.ReadToEnd();

            string[] gainValueTextList = gainMapText.Split(new char[] { ' ' });

            List<double> gainValueList = new List<double>();
            double gainValueTemp = 0;

            foreach (string gainValueText in gainValueTextList)
            {
                double.TryParse(gainValueText.Trim(), out gainValueTemp);

                gainValueList.Add(gainValueTemp);
            }

            streamReader.Close();
            fileStream.Close();

            double[] gainMap = gainValueList.ToArray();

            return gainMap;
        }

        private FileInfo[] GetFileList(string imageFolder)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(imageFolder);

            FileInfo[] fileInfos = directoryInfo.GetFiles();

            return fileInfos;
        }

        private void btnCorrectionNormal_Click(object sender, EventArgs e)
        {
            label4.Text = "Start!!";

            Thread thread = new Thread(new ThreadStart(func));
            thread.Name = "Thread";
            thread.IsBackground = true;

            thread.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtAirInput.Clear();
            string file_path = string.Empty;
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if(fileBrowser.ShowDialog() == DialogResult.OK)
            {
                file_path = fileBrowser.FileName;
                txtAirInput.Text = file_path;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            txtAirOutput.Clear();
            string file_path = string.Empty;
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                file_path = fileBrowser.FileName;
                txtAirOutput.Text = file_path;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtOffsetInput.Clear();
            string file_path = string.Empty;
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                file_path = fileBrowser.FileName;
                txtOffsetInput.Text = file_path;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            txtOffsetOutput.Clear();
            string file_path = string.Empty;
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                file_path = fileBrowser.FileName;
                txtOffsetOutput.Text = file_path;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            txtAirInput.Clear();
            string file_path = string.Empty;
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                file_path = fileBrowser.FileName;
                txtNormInput.Text = file_path;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtAirInput.Clear();
            string file_path = string.Empty;
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                file_path = fileBrowser.FileName;
                txtNormOutput.Text = file_path;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtImgInput.Text = folderBrowser.SelectedPath;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtImgOutput.Text = folderBrowser.SelectedPath;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                txtGainOutput.Text = folderBrowser.SelectedPath;
            }
        }

        private string GetConfigPath()
        {
            return string.Format("{0}\\CorrectionLibTester.ini", AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SaveConfig()
        {
            string configFile = GetConfigPath();

            IniFile ini = new IniFile();

            ini["IMAGE_INFO"]["HEIGHT"] = txtImageHeight.Text;
            ini["IMAGE_INFO"]["WIDTH"] = txtImageWidth.Text;

            ini["PATH_INFO"]["AIR_INPUT"] = txtAirInput.Text;
            ini["PATH_INFO"]["AIR_OUTPUT"] = txtAirOutput.Text;
            ini["PATH_INFO"]["OFFSET_INPUT"] = txtOffsetInput.Text;
            ini["PATH_INFO"]["OFFSET_OUTPUT"] = txtOffsetOutput.Text;
            ini["PATH_INFO"]["GAIN_OUTPUT"] = txtGainOutput.Text;
            ini["PATH_INFO"]["CORRECTION_INPUT"] = txtImgInput.Text;
            ini["PATH_INFO"]["CORRECTION_OUTPUT"] = txtImgOutput.Text;
            ini["PATH_INFO"]["NORM_INPUT"] = txtNormInput.Text;
            ini["PATH_INFO"]["NORM_OUTPUT"] = txtNormOutput.Text;
            
            ini.Save(configFile);
        }

        private void LoadConfig()
        {
            string configFile = GetConfigPath();

            if (File.Exists(configFile) == false)
            {
                return;
            }

            IniFile ini = new IniFile();

            ini.Load(configFile);

            txtImageHeight.Text = ini["IMAGE_INFO"]["HEIGHT"].ToString();
            txtImageWidth.Text = ini["IMAGE_INFO"]["WIDTH"].ToString();

            txtAirInput.Text = ini["PATH_INFO"]["AIR_INPUT"].ToString();
            txtAirOutput.Text = ini["PATH_INFO"]["AIR_OUTPUT"].ToString();
            txtOffsetInput.Text = ini["PATH_INFO"]["OFFSET_INPUT"].ToString();
            txtOffsetOutput.Text = ini["PATH_INFO"]["OFFSET_OUTPUT"].ToString();
            txtGainOutput.Text = ini["PATH_INFO"]["GAIN_OUTPUT"].ToString();
            txtImgInput.Text = ini["PATH_INFO"]["CORRECTION_INPUT"].ToString();
            txtImgOutput.Text = ini["PATH_INFO"]["CORRECTION_OUTPUT"].ToString();
            txtNormInput.Text = ini["PATH_INFO"]["NORM_INPUT"].ToString();
            txtNormOutput.Text = ini["PATH_INFO"]["NORM_OUTPUT"].ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            correction.RegistWriteLogHandler(WriteLog);

            LoadConfig();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfig();
        }

        private ushort[] BytesToUShorts(byte[] bytes)
        {
            ushort[] bufferInUShort = new ushort[bytes.Length / 2];

            Buffer.BlockCopy(bytes, 0, bufferInUShort, 0, bytes.Length);

            return bufferInUShort;
        }

        private ushort[] ReadBuffer(string filePath)
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);

            byte[] bufferInByte = new byte[fileStream.Length];

            bufferInByte = binaryReader.ReadBytes((int)fileStream.Length);

            return BytesToUShorts(bufferInByte);
        }

        private List<ushort[]> ReadAllRawFiles(string folderPath)
        {
            List<ushort[]> imageBufferList = new List<ushort[]>();

            DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);

            FileInfo[] fileInfos = directoryInfo.GetFiles();

            foreach (FileInfo fileInfo in fileInfos)
            {
                imageBufferList.Add(ReadBuffer(fileInfo.FullName));
            }

            return imageBufferList;
        }

        public void WriteBufferToFile(ushort[] imageBufferU16, string filePath)
        {
            string pathCheck = Path.GetDirectoryName(filePath);

            FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);

            BinaryWriter binaryWriter = new BinaryWriter(fileStream);

            byte[] imageBufferU8 = new byte[imageBufferU16.Length * 2];

            Buffer.BlockCopy(imageBufferU16, 0, imageBufferU8, 0, imageBufferU8.Length);

            binaryWriter.Write(imageBufferU8);

            binaryWriter.Close();

            fileStream.Close();
        }

        public void AverageImage(List<ushort[]> inImageBufferList, ref ushort[] outImageBuffer)
        {
            if (inImageBufferList == null
                || inImageBufferList.Count == 0)
            {
                return;
            } // else

            if (outImageBuffer == null)
            {
                return;
            } // else

            if (inImageBufferList[0].Length != outImageBuffer.Length)
            {
                return;
            } // else

            int imageLength = inImageBufferList[0].Length;

            int totalBufferCount = inImageBufferList.Count;

            for (int pixelIndex = 0; pixelIndex < imageLength; pixelIndex++)
            {
                decimal sumValue = 0;

                for (int averageCount = 0; averageCount < totalBufferCount; averageCount++)
                    sumValue += inImageBufferList[averageCount][pixelIndex];

                outImageBuffer[pixelIndex] = (ushort)(sumValue / totalBufferCount);
            }
        }

        private void btnMakeAir_Click(object sender, EventArgs e)
        {
            List<ushort[]> imageBufferList = ReadAllRawFiles(txtAirInput.Text);

            ushort[] outImageBuffer = new ushort[imageBufferList[0].Length];

            AverageImage(imageBufferList, ref outImageBuffer);

            WriteBufferToFile(outImageBuffer, txtAirOutput.Text);
        }

        private void btnMakeOffset_Click(object sender, EventArgs e)
        {
            List<ushort[]> imageBufferList = ReadAllRawFiles(txtOffsetInput.Text);

            ushort[] outImageBuffer = new ushort[imageBufferList[0].Length];

            AverageImage(imageBufferList, ref outImageBuffer);

            WriteBufferToFile(outImageBuffer, txtOffsetOutput.Text);
        }

        private int ToInteger(string input)
        {
            int output = 0;

            int.TryParse(input, out output);

            return output;
        }

        public void WriteFloatArrayToFile(float[] floatArray, string filePath)
        {
            string pathCheck = Path.GetDirectoryName(filePath);

            FileStream fileStream = null;
            StreamWriter streamWriter = null;

            fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            streamWriter = new StreamWriter(fileStream);

            StringBuilder floatValueText = new StringBuilder();
            bool first = true;
            foreach (double floatValue in floatArray)
            {
                if (first == false) floatValueText.Append(" ");

                floatValueText.Append(string.Format("{0:f6}", floatValue));

                first = false;
            }

            streamWriter.Write(floatValueText);

            streamWriter.Close();
            fileStream.Close();
        }

        private void WriteFloatArrayToRawFile(float[] gainMap, string filePath)
        {
            FileStream fileStream = null;
            BinaryWriter binaryWriter = null;

            fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            binaryWriter = new BinaryWriter(fileStream);

            byte[] bufferInBytes = new byte[gainMap.Length * 4];

            Buffer.BlockCopy(gainMap, 0, bufferInBytes, 0, bufferInBytes.Length);

            binaryWriter.Write(bufferInBytes);

            binaryWriter.Close();
            fileStream.Close();
        }

        private void SetOffsetImage()
        {
            int width = ToInteger(txtImageWidth.Text);
            int height = ToInteger(txtImageHeight.Text);

            correction.SetImageSize(width, height);

            if (File.Exists(txtOffsetOutput.Text) == false)
            {
                btnMakeAir_Click(null, null);
            }

            ushort[] offsetImageBuffer = ReadBuffer(txtOffsetOutput.Text);

            correction.SetOffsetImage(offsetImageBuffer, width, height);
        }

        private void SetAirImage()
        {
            int width = ToInteger(txtImageWidth.Text);
            int height = ToInteger(txtImageHeight.Text);

            correction.SetImageSize(width, height);

            if (File.Exists(txtAirOutput.Text) == false)
            {
                btnMakeOffset_Click(null, null);
            }

            ushort[] airImageBuffer = ReadBuffer(txtAirOutput.Text);

            correction.SetAirImage(airImageBuffer, width, height);
        }

        private void btnMakeGainMap_Click(object sender, EventArgs e)
        {
            int width = ToInteger(txtImageWidth.Text);
            int height = ToInteger(txtImageHeight.Text);

            correction.SetImageSize(width, height);

            SetOffsetImage();

            SetAirImage();

            correction.CreateGainMap();

            float[] gainMapBuffer = correction.GetGainMap(width, height);

            WriteFloatArrayToFile(gainMapBuffer, txtGainOutput.Text);

            WriteFloatArrayToRawFile(gainMapBuffer, txtGainOutput.Text + ".raw");
        }

        private void SetGainMap()
        {
            int width = ToInteger(txtImageWidth.Text);
            int height = ToInteger(txtImageHeight.Text);

            if (File.Exists(txtGainOutput.Text) == false)
            {
                btnMakeGainMap_Click(null, null);
            }

            float[] gainMapBuffer = ReadFloatArrayFromFile(txtGainOutput.Text);

            correction.SetGainMap(gainMapBuffer);
        }

        public float[] ReadFloatArrayFromFile(string filePath)
        {
            FileStream fileStream = null;
            StreamReader streamReader = null;

            fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            streamReader = new StreamReader(fileStream);

            string arrayText = streamReader.ReadToEnd();

            string[] floatValueTextList = arrayText.Split(' ');

            List<float> floatValueList = new List<float>();
            float floatValueTemp = 0;

            foreach (string floatValueText in floatValueTextList)
            {
                float.TryParse(floatValueText.Trim(), out floatValueTemp);

                floatValueList.Add(floatValueTemp);
            }

            streamReader.Close();
            fileStream.Close();

            float[] floatArray = floatValueList.ToArray();

            return floatArray;
        }

        private void btnApplyNormSmooth_Click(object sender, EventArgs e)
        {
            int width = ToInteger(txtImageWidth.Text);
            int height = ToInteger(txtImageHeight.Text);
            
            ushort[] normPhantomBuffer = ReadBuffer(txtNormInput.Text);

            short[] smoothBuffer = correction.ApplySplineSmooth(normPhantomBuffer, width, height);

            WriteBufferToFile(txtNormOutput.Text, smoothBuffer);
        }
    }
}
