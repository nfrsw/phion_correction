﻿namespace NFR_Correction_TestCS
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCorrectionNormal = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAirInput = new System.Windows.Forms.TextBox();
            this.txtOffsetInput = new System.Windows.Forms.TextBox();
            this.txtImgInput = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtImageWidth = new System.Windows.Forms.TextBox();
            this.txtImageHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txtGainOutput = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnMakeAir = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.txtAirOutput = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.txtOffsetOutput = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnMakeOffset = new System.Windows.Forms.Button();
            this.btnMakeGainMap = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.txtImgOutput = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtNormOutput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.txtNormInput = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnApplyNormSmooth = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCorrectionNormal
            // 
            this.btnCorrectionNormal.Location = new System.Drawing.Point(519, 328);
            this.btnCorrectionNormal.Name = "btnCorrectionNormal";
            this.btnCorrectionNormal.Size = new System.Drawing.Size(166, 26);
            this.btnCorrectionNormal.TabIndex = 0;
            this.btnCorrectionNormal.Text = "Correction Normal";
            this.btnCorrectionNormal.UseVisualStyleBackColor = true;
            this.btnCorrectionNormal.Click += new System.EventHandler(this.btnCorrectionNormal_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "AIr Input : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Off Input : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "Image In Path : ";
            // 
            // txtAirInput
            // 
            this.txtAirInput.Location = new System.Drawing.Point(122, 36);
            this.txtAirInput.Name = "txtAirInput";
            this.txtAirInput.Size = new System.Drawing.Size(503, 21);
            this.txtAirInput.TabIndex = 4;
            // 
            // txtOffsetInput
            // 
            this.txtOffsetInput.Location = new System.Drawing.Point(122, 92);
            this.txtOffsetInput.Name = "txtOffsetInput";
            this.txtOffsetInput.Size = new System.Drawing.Size(503, 21);
            this.txtOffsetInput.TabIndex = 5;
            // 
            // txtImgInput
            // 
            this.txtImgInput.Location = new System.Drawing.Point(122, 230);
            this.txtImgInput.Name = "txtImgInput";
            this.txtImgInput.Size = new System.Drawing.Size(503, 21);
            this.txtImgInput.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(631, 36);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(631, 92);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(631, 230);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(54, 23);
            this.button4.TabIndex = 9;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(122, 286);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(563, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(14, 407);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(671, 317);
            this.txtLog.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 392);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "Log";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "Image Size :";
            // 
            // txtImageWidth
            // 
            this.txtImageWidth.Location = new System.Drawing.Point(122, 10);
            this.txtImageWidth.Name = "txtImageWidth";
            this.txtImageWidth.Size = new System.Drawing.Size(54, 21);
            this.txtImageWidth.TabIndex = 15;
            this.txtImageWidth.Text = "664";
            this.txtImageWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtImageHeight
            // 
            this.txtImageHeight.Location = new System.Drawing.Point(201, 10);
            this.txtImageHeight.Name = "txtImageHeight";
            this.txtImageHeight.Size = new System.Drawing.Size(54, 21);
            this.txtImageHeight.TabIndex = 16;
            this.txtImageHeight.Text = "664";
            this.txtImageHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(182, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "X";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(631, 202);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(54, 23);
            this.button5.TabIndex = 20;
            this.button5.Text = "...";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtGainOutput
            // 
            this.txtGainOutput.Location = new System.Drawing.Point(122, 202);
            this.txtGainOutput.Name = "txtGainOutput";
            this.txtGainOutput.Size = new System.Drawing.Size(503, 21);
            this.txtGainOutput.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "Gain Output : ";
            // 
            // btnMakeAir
            // 
            this.btnMakeAir.Location = new System.Drawing.Point(122, 328);
            this.btnMakeAir.Name = "btnMakeAir";
            this.btnMakeAir.Size = new System.Drawing.Size(119, 26);
            this.btnMakeAir.TabIndex = 21;
            this.btnMakeAir.Text = "Make Air";
            this.btnMakeAir.UseVisualStyleBackColor = true;
            this.btnMakeAir.Click += new System.EventHandler(this.btnMakeAir_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(631, 64);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(54, 23);
            this.button7.TabIndex = 24;
            this.button7.Text = "...";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // txtAirOutput
            // 
            this.txtAirOutput.Location = new System.Drawing.Point(122, 64);
            this.txtAirOutput.Name = "txtAirOutput";
            this.txtAirOutput.Size = new System.Drawing.Size(503, 21);
            this.txtAirOutput.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 12);
            this.label9.TabIndex = 22;
            this.label9.Text = "AIr Output : ";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(631, 119);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(54, 23);
            this.button8.TabIndex = 27;
            this.button8.Text = "...";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // txtOffsetOutput
            // 
            this.txtOffsetOutput.Location = new System.Drawing.Point(122, 119);
            this.txtOffsetOutput.Name = "txtOffsetOutput";
            this.txtOffsetOutput.Size = new System.Drawing.Size(503, 21);
            this.txtOffsetOutput.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 12);
            this.label10.TabIndex = 25;
            this.label10.Text = "Off Output : ";
            // 
            // btnMakeOffset
            // 
            this.btnMakeOffset.Location = new System.Drawing.Point(247, 328);
            this.btnMakeOffset.Name = "btnMakeOffset";
            this.btnMakeOffset.Size = new System.Drawing.Size(119, 26);
            this.btnMakeOffset.TabIndex = 28;
            this.btnMakeOffset.Text = "Make Offset";
            this.btnMakeOffset.UseVisualStyleBackColor = true;
            this.btnMakeOffset.Click += new System.EventHandler(this.btnMakeOffset_Click);
            // 
            // btnMakeGainMap
            // 
            this.btnMakeGainMap.Location = new System.Drawing.Point(122, 360);
            this.btnMakeGainMap.Name = "btnMakeGainMap";
            this.btnMakeGainMap.Size = new System.Drawing.Size(244, 26);
            this.btnMakeGainMap.TabIndex = 29;
            this.btnMakeGainMap.Text = "Make Gain Map";
            this.btnMakeGainMap.UseVisualStyleBackColor = true;
            this.btnMakeGainMap.Click += new System.EventHandler(this.btnMakeGainMap_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(631, 257);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(54, 23);
            this.button9.TabIndex = 33;
            this.button9.Text = "...";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // txtImgOutput
            // 
            this.txtImgOutput.Location = new System.Drawing.Point(122, 257);
            this.txtImgOutput.Name = "txtImgOutput";
            this.txtImgOutput.Size = new System.Drawing.Size(503, 21);
            this.txtImgOutput.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 260);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(104, 12);
            this.label11.TabIndex = 31;
            this.label11.Text = "Image Out Path : ";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(631, 173);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 23);
            this.button1.TabIndex = 39;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtNormOutput
            // 
            this.txtNormOutput.Location = new System.Drawing.Point(122, 173);
            this.txtNormOutput.Name = "txtNormOutput";
            this.txtNormOutput.Size = new System.Drawing.Size(503, 21);
            this.txtNormOutput.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 12);
            this.label4.TabIndex = 37;
            this.label4.Text = "Norm Output : ";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(631, 146);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(54, 23);
            this.button6.TabIndex = 36;
            this.button6.Text = "...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtNormInput
            // 
            this.txtNormInput.Location = new System.Drawing.Point(122, 146);
            this.txtNormInput.Name = "txtNormInput";
            this.txtNormInput.Size = new System.Drawing.Size(503, 21);
            this.txtNormInput.TabIndex = 35;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 149);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 12);
            this.label12.TabIndex = 34;
            this.label12.Text = "Norm Input : ";
            // 
            // btnApplyNormSmooth
            // 
            this.btnApplyNormSmooth.Location = new System.Drawing.Point(519, 360);
            this.btnApplyNormSmooth.Name = "btnApplyNormSmooth";
            this.btnApplyNormSmooth.Size = new System.Drawing.Size(166, 26);
            this.btnApplyNormSmooth.TabIndex = 40;
            this.btnApplyNormSmooth.Text = "Phantom Smooth";
            this.btnApplyNormSmooth.UseVisualStyleBackColor = true;
            this.btnApplyNormSmooth.Click += new System.EventHandler(this.btnApplyNormSmooth_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 736);
            this.Controls.Add(this.btnApplyNormSmooth);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtNormOutput);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.txtNormInput);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.txtImgOutput);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnMakeGainMap);
            this.Controls.Add(this.btnMakeOffset);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.txtOffsetOutput);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.txtAirOutput);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnMakeAir);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtGainOutput);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtImageHeight);
            this.Controls.Add(this.txtImageWidth);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtImgInput);
            this.Controls.Add(this.txtOffsetInput);
            this.Controls.Add(this.txtAirInput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCorrectionNormal);
            this.Name = "Form1";
            this.Text = "Correction Tester";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCorrectionNormal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAirInput;
        private System.Windows.Forms.TextBox txtOffsetInput;
        private System.Windows.Forms.TextBox txtImgInput;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtImageWidth;
        private System.Windows.Forms.TextBox txtImageHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtGainOutput;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnMakeAir;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox txtAirOutput;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox txtOffsetOutput;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnMakeOffset;
        private System.Windows.Forms.Button btnMakeGainMap;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox txtImgOutput;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtNormOutput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtNormInput;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnApplyNormSmooth;
    }
}

